package com.cpl_cursos.java.ejemplos.spring2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEjercicio2InydepApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEjercicio2InydepApplication.class, args);
	}

}
