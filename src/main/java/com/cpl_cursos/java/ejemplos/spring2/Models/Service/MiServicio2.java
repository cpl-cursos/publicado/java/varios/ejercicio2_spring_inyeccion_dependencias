package com.cpl_cursos.java.ejemplos.spring2.Models.Service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/*
    PARA DESACOPLAR, hay que pasarla a un componente de Spring. Para ello la anotamos con @Component.
    Con eso queda registrada como un beans de Spring y dentro del contexto.

    Se crea una sola vez y se puede inyectar en cualquier componente del contexto (todo lo que hay dentro del paquete
    base o principal, en nuestro caso com/cpl_cursos/java/ejemplos/spring2)

    También se puede utilizar cualquier decorador derivado de un componente como @Service
 */
//@Component("Mi_servicio2") //<-- Ahora si es necesario ya que hay DOS implementaciones de "IfxServicio.operacion()"
//@Primary    //<-- Si queremos que sea la utilizada por defecto
public class MiServicio2 implements IfxServicio{

    @Override  //<-- Al ser una implementación de una interfaz, hay que sobreescribir el método definido en aquellla
    public String operacion() {
        return "Realizando cálculos en la operación de MiServicio2";
    }

}
