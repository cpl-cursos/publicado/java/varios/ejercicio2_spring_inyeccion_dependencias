package com.cpl_cursos.java.ejemplos.spring2.Controllers;

import com.cpl_cursos.java.ejemplos.spring2.Models.Service.IfxServicio;
import com.cpl_cursos.java.ejemplos.spring2.Models.Service.MiServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    /*  PASO 1
        Vamos a utilizar el servicio que hemos creado. Primero sin inyectar dependencia. Es decir, creamos una instancia
        del mismo como atrinuto de la clase.

        Como consecuencia, el servicio está completamente acoplado al código.

        Si el servico cambia, hay que actualizarlo en cada una de las instancias en las que se consuma
     */
    //private MiServicio servicio = new MiServicio(); //<-- MiServicio esta fuertemente acoplado al código del controlador

    /*  PASO 2
        Al desacoplar un poco el servicio, ya no es necesario crear una instancia pues Spring lo va a inyectar
        automáticamente.
        Esto se consigue con la anotación @AutoWired, que busca en el contenedor una instancia de la clase indicada
     */
    //@Autowired
    //private MiServicio servicio;

    /* PASO 3
        Al haber convertido el servicio en una interfaz con su implementación -o implementaciones correspondientes-,
        tenemos que inyectar la interfaz y no su implementación, con lo que conseguimos desacoplar el servicio ya que la
        interfaz no cambiará - es genérica- aunque lo haga la implementación.

        Si hay distintas implementaciones, usaramemos la anotación @Qualifier para identificarla según el nombre
        asignado en la implementación.

        Cuando sólo hay una implementación NO ES NECESARIO @Qualifier. Se incluye aquí como ejemplo de uso
     */
    //@Autowired
    //@Qualifier("Mi_servicio") //<-- Paso 7. LA otra implementación es la anotada como @Primary, pero se ejecutará esta
    //private IfxServicio servicio;

    // PASO 5 - Vamos inyectar mediante el constructor...
    // PASO 6 - Como hay dos implementaciones, si no se indica nada Spring escoge la anotada con @Primary (en este paso, es MiServicio2)
    //@Autowired //<-- NUEVO. Cuando lo inyecta el constructor, no es necesario el @AutoWired. Se pone como ejemplo
    //public IndexController(IfxServicio servicio) {
    //    this.servicio = servicio;
    //}

    // PASO 8 - Con @Beans
    @Autowired
    private IfxServicio servicio;

    // Vamos a consumir el servicio en estas rutas
    @GetMapping({"/", "/index"})
    public String index(Model modelo) {
        modelo.addAttribute("operacion", servicio.operacion());
        return "index";
    }

    /* PASO 4
        Si queremos inyectar la dependencia mediante el setter, lo anotamos como @Autowired en lugar de anotar
        la propiedad
     */
    //@Autowired
    //public void setServicio(IfxServicio servicio) {
    //    this.servicio = servicio;
    //}
}
