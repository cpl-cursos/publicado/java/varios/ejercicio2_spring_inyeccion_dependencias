package com.cpl_cursos.java.ejemplos.spring2.Models.Service;

import org.springframework.stereotype.Component;

/*
    PARA DESACOPLAR, hay que pasarla a un componente de Spring. Para ello la anotamos con @Component.
    Con eso queda registrada como un beans de Spring y dentro del contexto.

    Se crea una sola vez y se puede inyectar en cualquier componente del contexto (todo lo que hay dentro del paquete
    base o principal, en nuestro caso com/cpl_cursos/java/ejemplos/spring2)

    También se puede utilizar cualquier decorador derivado de un componente como @Service
 */
//@Component("Mi_servicio") //<-- Para identificar la implementación. El nombre es usado por @Qualifier. Si sólo hay una implementación, no es necesario
public class MiServicio implements IfxServicio{

    @Override  //<-- Al ser una implementación de una interfaz, hay que sobreescribir el método definido en aquellla
    public String operacion() {
        //return "Realizando alguna operación de lógica de negocio y/o de un DAO, de un API Rest, microservicio...";

        // Utilizamos este otro mensaje para identificar esta otra fase de desacoplamiento
        //return "Realizando alguna operación de lógica de negocio y/o de un DAO, de un API Rest, microservicio... Pero algo más desacoplado";

        // Cambiamos el mensaje para distinguirlo del paso anterior
        //return "Realizando alguna operación de lógica de negocio y/o de un DAO, de un API Rest, microservicio... Ahora ya desacoplado";

        // Ahora es para identificar que lo devuelve el setter
        //return "Realizando alguna operación de lógica de negocio y/o de un DAO, de un API Rest, microservicio... Ahora desde el setter";

        // Ahora es para identificar que lo devuelve el constructor
        return "Realizando alguna operación de lógica de negocio y/o de un DAO, de un API Rest, microservicio... Ahora desde el constructor";
    }

    /*
        Una clase "Componente" de Spring DEBE tener siempre un constructor vacío. Si no hay constructor definido,
        Spring asume por defecto un constructor vacío, pero si hubiera definido un constructor con parámetros, SERÍA
        OBLIGATORIO definir un constructor vacío (sin parámetros). Si no se hace, se genera un error.
     */
}
