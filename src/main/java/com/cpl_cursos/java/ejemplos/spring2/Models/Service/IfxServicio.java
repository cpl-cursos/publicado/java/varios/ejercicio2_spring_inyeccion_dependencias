package com.cpl_cursos.java.ejemplos.spring2.Models.Service;


/*
    Para conseguir un desacoplamiento completo entre el servicio y el controlador, creamos una interfaz.

    Esto permite definir comportamientos -métodos- abstractos que posteriormente pueden ser implementados de diversas
    formas.

    En el controlador se inyectará la interfaz, que siempre va a estar definida, al ser abstracta. De esa forma,
    el controlador es ajeno a cualquier cambio que se pueda producir en el servicio
 */
public interface IfxServicio {

    public String operacion();
}
