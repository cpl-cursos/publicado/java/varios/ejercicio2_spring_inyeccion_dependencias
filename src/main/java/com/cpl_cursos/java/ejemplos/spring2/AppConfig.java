package com.cpl_cursos.java.ejemplos.spring2;

import com.cpl_cursos.java.ejemplos.spring2.Models.Service.IfxServicio;
import com.cpl_cursos.java.ejemplos.spring2.Models.Service.MiServicio;
import com.cpl_cursos.java.ejemplos.spring2.Models.Service.MiServicio2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class AppConfig {

    @Bean("Mi_servicio") //<-- Si hay más de uno igual, se le puede asignar un nombre
    @Primary
    public IfxServicio registrarMiServicio() {
        return new MiServicio();
    }

    @Bean("Mi_servicio2") //<-- Si hay más de uno igual, se le puede asignar un nombre
    public IfxServicio registrarMiServicio2() {
        return new MiServicio2();
    }
}
